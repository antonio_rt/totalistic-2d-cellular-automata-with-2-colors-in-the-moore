# Outer Totalistic 2D Cellular Automata with 2 colors in the Moore Neighborhood #

Mathematica notebook used to explore outer totalistic 2D CA in the Moore neighborhood, using Wolfram's numbering scheme. Notebook based in sample app by Vitaliy Kaurov. The app allows the specification of random initial conditions, yet it's predefined on an asymmetric condition that eases the visualization of complex behavior during evolution.  

The notebook contains interesting automata with complex behavior (class IV) found with the app.

A video showing the evolution of Rule 181411, found with the notebook, can be found [here](http://youtu.be/rmLnrvt8i50).